TCPinfo - Python3 extension to collect information about TCP-sockets
===========================================================================================================
TCPinfo is a Python3 extension to collect information about TCP-sockets via the Linux's inet_diag interface

List of available fields
--------------------------------------------------------------------------------
45 available fields:

state, ca_state, srcIp, srcPort, dstIp, dstPort, cwnd, bytes_acked, snd_ssthresh, rtt, retransmits, probes, backoff, lost, snd_wscale, rcv_wscale, rto, ato, snd_mss, rcv_mss, unacked, sacked, retrans, fackets, last_data_sent, last_ack_sent, last_data_recv, last_ack_recv, pmtu, rcv_ssthresh, rttvar, advmss, reordering, rcv_rtt, rcv_space, total_retrans, pacing_rate, max_pacing_rate, bytes_received, segs_out, segs_in, user, uid, inode, options

Example
--------------------------------------------------------------------------------
{'state': 'ESTABLISHED', 'srcIp': '192.168.123.42', 'srcPort': 34226, 'dstIp': '123.456.78.90', 'dstPort': 123, 'cwnd': 7, 'bytes_acked': 13426, 'snd_ssthresh': 7, 'rtt': 26380, 'lost': 0, 'retransmits': 0, 'probes': 0, 'backoff': 0, 'snd_wscale': 7, 'rcv_wscale': 7, 'rto': 227000, 'ato': 40000, 'snd_mss': 1392, 'rcv_mss': 1352, 'unacked': 0, 'sacked': 0, 'retrans': 0, 'fackets': 0, 'last_data_sent': 18646, 'last_ack_sent': 0, 'last_data_recv': 18627, 'last_ack_recv': 18627, 'pmtu': 1500, 'rcv_ssthresh': 184792, 'rttvar': 7833, 'advmss': 1448, 'reordering': 3, 'rcv_rtt': 28466, 'rcv_space': 35152, 'total_retrans': 3, 'pacing_rate': 443236, 'max_pacing_rate': 18446744073709551615, 'bytes_received': 141915, 'segs_out': 388, 'segs_in': 385, 'user': 'myusername', 'uid': 1000, 'inode': 1108935, 'options': ['ts', 'sack']}


Tested on
--------------------------------------------------------------------------------
Tested with GNU/Linux Kernel 4.{4-15}

Requires at least Kernel 4.1


Installation of TCPinfo:
--------------------------------------------------------------------------------
* system-wide installation:
    * sudo pip3 install .
* local installation:
    * pip3 install --user .


Compilation requirements:
--------------------------------------------------------------------------------
* Python.h      --> found in package "python3-dev" (Ubuntu) or similiar


Available functions:
--------------------------------------------------------------------------------
* startUp()
* getTcpInfoList()
* getListOfAvailableValues()
* tearDown()

Used
--------------------------------------------------------------------------------
* Used by "TCPlog" (see https://git.scc.kit.edu/CPUnetLOG/TCPlog/)
